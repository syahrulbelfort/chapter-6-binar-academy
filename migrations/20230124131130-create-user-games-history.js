'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserGamesHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      player_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE', // untuk kasih konfig apabila data member dihapus maka data loan dihapus
        references: {
          model: 'user_games',
          key: 'id',
        },
      },
      bio_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE', // untuk kasih konfig apabila data member dihapus maka data loan dihapus
        references: {
          model: 'UserGamesBiodata',
          key: 'id',
        },
      },
      condition: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserGamesHistories');
  },
};
