const express = require('express');
const router = express.Router();

//login routes
const loginController = require('../controllers/login');
router.get('/', loginController.index);
router.post('/', loginController.validate);

//home routes
const homeController = require('../controllers/home');
router.get('/:id/:nama', homeController.show);

//Dashboard users routes
const userController = require('../controllers/users');
router.get('/users', userController.index);
router.get('/users/:nama', userController.show);

//updates users
router.get('/edit/:id/:nama', userController.showEdit);
router.post('/edit/:id/:nama', userController.edit);

//register routes
const registerController = require('../controllers/register');
router.get('/register', registerController.index);
//create users
router.post('/register', registerController.create);

//delete users
const deleteController = require('../controllers/delete');
router.post('/delete/:id/', deleteController.index);

//about routes
const aboutController = require('../controllers/about');
router.get('/about', aboutController.index);

module.exports = router;
