const express = require('express');
const router = express.Router();
const { UserGamesBiodata } = require('../models');

router.get('/', (req, res) => {
  UserGamesBiodata.findAll().then((bio) => {
    res.status(200).json({
      status: 'success',
      data: bio,
    });
  });
});

router.post('/', (req, res) => {
  const { jenis_kelamin, tanggal_lahir, umur } = req.body;
  UserGamesBiodata.create({
    jenis_kelamin,
    tanggal_lahir,
    umur,
  }).then((bio) => {
    res.status(200).json({
      status: 'success',
      data: bio,
      message: 'Biodata berhasil di tambahkan!',
    });
  });
});

module.exports = router;
