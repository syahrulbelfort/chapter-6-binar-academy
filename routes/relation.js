const express = require('express');
const router = express.Router();
const { UserGamesHistory, User_game, UserGamesBiodata } = require('../models');

router.get('/', (req, res) => {
  UserGamesHistory.findAll({
    include: [
      {
        model: User_game,
      },
      {
        model: UserGamesBiodata,
      },
    ],
  }).then((history) => {
    res.status(200).json({
      status: 'success',
      data: history,
      message: 'ini adalah data history game',
    });
  });
});

router.post('/', (req, res) => {
  const { player_id, bio_id, condition } = req.body;
  UserGamesHistory.create({
    player_id,
    bio_id,
    condition,
  }).then((history) => {
    res.status(200).json({
      status: 'success',
      data: history,
      message: 'Data history permainan berhasil di tambahkan!',
    });
  });
});

module.exports = router;
