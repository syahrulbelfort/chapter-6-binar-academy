const { User_game } = require('../models');

module.exports = {
  index: (req, res) => {
    res.render('register');
  },
  create: (req, res) => {
    User_game.create({
      nama: req.body.nama,
      email: req.body.email,
      password: req.body.password,
    }).then((user_game) => {
      res.redirect('/users');
    });
  },
};
