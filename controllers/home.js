const { User_game } = require('../models');

module.exports = {
  index: (req, res) => {
    res.redirect('/users');
  },
  show: (req, res) => {
    User_game.findOne({
      where: { nama: req.params.nama },
    }).then((users) => {
      res.render('detail', { users });
    });
  },
};
