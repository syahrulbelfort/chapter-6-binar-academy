const { User_game } = require('../models');

const express = require('express');

module.exports = {
  index: (req, res) => {
    User_game.findAll().then((users) => {
      res.render('user', { users });
    });
  },
  show: (req, res) => {
    User_game.findOne({
      where: { nama: req.params.nama },
    }).then((users) => {
      res.render('detail', { users });
    });
  },
  showEdit: (req, res) => {
    User_game.findOne({
      where: { id: req.params.id, nama: req.params.nama },
    }).then((user) => {
      res.render('edit', { user });
    });
  },
  edit: (req, res) => {
    User_game.update(
      {
        nama: req.body.nama,
        email: req.body.email,
      },
      {
        where: { id: req.params.id, nama: req.params.nama },
      }
    ).then(() => {
      res.redirect('/users');
    });
  },
};
