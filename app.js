const express = require('express');
const app = express();
const port = 3100;
const ejs = require('ejs');

//VIEW ENGINE EJS
app.set('view engine', 'ejs');

//BUILT IN MIDDLEWARE
app.use(express.static('public'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
const routes = require('./routes/routes');

//ALL ROUTES
app.use('/', routes);

//CREATE & READ A BIODATA
const bioRoute = require('./routes/bio');
app.use('/bio', bioRoute);

// //CREATE USER HISTORY RELATION DATABASE PK
const relationRoute = require('./routes/relation');
app.use('/relation', relationRoute);

//LISTEN PORT
app.listen(port, () => {
  console.log(`server is running on ${port}`);
});
