'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGamesHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User_game, {
        foreignKey: `player_id`,
      });

      this.belongsTo(models.UserGamesBiodata, {
        foreignKey: 'bio_id',
        onDelete: 'CASCADE',
      });
    }
  }
  UserGamesHistory.init(
    {
      player_id: DataTypes.INTEGER,
      bio_id: DataTypes.INTEGER,
      condition: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'UserGamesHistory',
    }
  );
  return UserGamesHistory;
};
