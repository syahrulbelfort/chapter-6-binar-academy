'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGamesBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.UserGamesHistory, {
        foreignKey: 'bio_id',
      });
    }
  }
  UserGamesBiodata.init(
    {
      jenis_kelamin: DataTypes.STRING,
      tanggal_lahir: DataTypes.INTEGER,
      umur: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'UserGamesBiodata',
    }
  );
  return UserGamesBiodata;
};
